// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PointziSDK",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "StreetHawkCore_Pointzi",
            targets: ["StreetHawkCore_Pointzi"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
         .package(url: "https://github.com/SVGKit/SVGKit", from: "3.0.0"),
    ],
    
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
         .binaryTarget(
            name: "StreetHawkCore_Pointzi",
            url: "https://repo.pointzi.com/sdk/pointzi/ios/spm/2.4.0/StreetHawkCore_Pointzi.xcframework.zip",
            checksum: "f4dcd096d251dd463296138f2d28b57bad1466a098a020e8280ef3ec9e7d38a7" ),
        .target(name:"PointziSDK",
                resources: [
                .copy("PointziSDK.bundle")
            ]
//                linkerSettings: [
//                        .linkedFramework("SystemConfiguration"),
//                        .linkedFramework("QuartzCore"),
//                        .linkedFramework("CoreImage"),
//                        .linkedFramework("CoreText"),
//                        .linkedFramework("WebKit"),
//                        .linkedFramework("UserNotifications"),
//                        .linkedFramework("CoreTelephony", .when(platforms: [.iOS])),
//                        .linkedLibrary("z"),
//                      ]
         )
        
        
    ]
)

/*
let package = Package(
    name: "PSPDFKit",
    products: [
        .library(
            name: "PSPDFKit",
            targets: ["PSPDFKit", "PSPDFKitUI"]),
    ],
    targets: [
        .binaryTarget(
            name: "PSPDFKit",
            url: "https://customers.pspdfkit.com/pspdfkit/xcframework/10.5.0.zip",
            checksum: "d777a6f6f8c6710471f33dbad870c1ae448aae2a8de23a8629dd167b4b11d83f"),
        .binaryTarget(
            name: "PSPDFKitUI",
            url: "https://customers.pspdfkit.com/pspdfkitui/xcframework/10.5.0.zip",
            checksum: "bd7be965c275096fab838e6cf296a82ce969c7acc23dc7adf12b963ead70ea1e"),
    ]
)
*/
