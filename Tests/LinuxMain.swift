import XCTest

import PointziSDKTests

var tests = [XCTestCaseEntry]()
tests += PointziSDKTests.allTests()
XCTMain(tests)
